# Spotify Label
This is a quick and dirty GNOME shell extension to show the currently playing song on Spotify in the top bar.

This fork moves the default position to be right of the clock (aligned center) rather than left of the clock (aligned left).

## Installation
1.	`git clone git@gitlab.com:gs/gnome-shell-spotify-label.git ~/.local/share/gnome-shell/extensions/spotifylabel@mheine.se`.
1.	Restart GNOME shell with `Alt+F2, r`.
1.	Enable the extension in `gnome-tweaks`.

## License
This project is licensed under the MIT license. See [LICENSE](LICENSE).
